;;; ox-mahtml.el --- Ma_124's HTML Backend for Org -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Ma_124 <ma_124+oss@pm.me>

;; Author: Ma_124 <ma_124+oss@pm.me>
;; Keywords: org, html

;;; Commentary:
;;
;; This adds a new HTML exporter tailored to the needs of Ma_124.

;;; Code:

(require 'ox-html)

; TODO
; - [ ] MathJax 3.0
; - [ ] Name and Date under Title

(org-export-define-derived-backend 'mahtml 'html
  :menu-entry '(?a "Export to Ma_124's HTML"
                   ((?A "To temporary buffer" (lambda (a s v b) (org-mahtml-export-to-buffer    a s v)))
                    (?a "To file"             (lambda (a s v b) (org-mahtml-export-to-file      a s v)))
                    (?o "To file and open"    (lambda (a s v b) (org-mahtml-export-to-file-open a s v)))))
  :options-alist '((:latex-header "LATEX_HEADER" nil nil newline))
  :translate-alist '((inner-template . org-mahtml-inner-template)))

(defun org-mahtml-inner-template (contents info)
  "Return body of document string after HTML conversion.
CONTENTS is the transcoded contents string.  INFO is a plist
holding export options."
  (concat (if (plist-get info :latex-header)
              (concat "<p style=\"display: none\">\\(" (plist-get info :latex-header) "\\)</p>")
              "")
          (org-html-inner-template contents info)))

(defvar org-mahtml-mathjax-template
  org-html-mathjax-template
  "The MathJax template.  See also `org-html-mathjax-options'.")

(defvar org-mahtml-head
  (concat "<style>"
          (with-temp-buffer
            (insert-file-contents (concat (file-name-directory load-file-name) "dist.css"))
            (buffer-string))
          "</style>")
  "Head definitions for exported HTML files.")

(defmacro org-mahtml--in-context (&rest body)
  "Run BODY in the appropriate context."
  `(let (
         (org-html-postamble nil)
         (org-html-htmlize-font-prefix "code-")
         (org-html-head org-mahtml-head)
         (org-html-head-include-default-style nil)
         (org-html-htmlize-output-type 'css)
         (org-html-mathjax-template org-mahtml-mathjax-template)
         (org-html-html5-fancy t)
         (org-html-doctype "html5")
         (org-html-with-latex 'mathjax)
         (org-html-checkbox-type 'html))
    (progn ,@body)))

;;; Export Functions
;;;###autoload
(defun org-mahtml-export-to-buffer (&optional a s v)
  "Export current buffer to a new buffer with Ma_124's HTML."
  (interactive)
  (org-mahtml--in-context
   (org-export-to-buffer 'mahtml "*Ort Ma HTML Export*" a s v nil nil (lambda () (text-mode)))))

;;;###autoload
(defun org-mahtml-export-to-file (&optional a s v)
  "Export current buffer to a new file with Ma_124's HTML."
  (interactive)
  (let ((outfile (org-export-output-file-name ".html" s)))
    (org-mahtml--in-context
     (org-export-to-file 'mahtml outfile a s v)
     outfile)))

;;;###autoload
(defun org-mahtml-export-to-file-open (&optional _a s v)
  "Export current buffer to new file Ma_124's HTML and then open that file."
  (interactive)
  (org-open-file (org-mahtml-export-to-file nil s v)))

(provide 'ox-mahtml)

;;; ox-mahtml.el ends here
