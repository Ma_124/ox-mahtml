const plugin = require('tailwindcss/plugin');
const svgToDataUri = require("mini-svg-data-uri");

module.exports = {
  content: [],
  theme: {
    extend: {},
  },
  plugins: [
    require("@tailwindcss/typography"),
    plugin(function({ addUtilities, addComponents, e, prefix, config }) {
      addComponents({
        ".box-checked": {
          "background-image": `url("${svgToDataUri(`<svg viewBox="0 0 16 16" fill="white" xmlns="http://www.w3.org/2000/svg"><path d="M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z"/></svg>`)}")`
        },
        ".box-trans": {
          "background-image": `url("${svgToDataUri(`<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 16"><path stroke="white" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 8h8"/></svg>`)}")`
        }
      });
    }),
  ],
}
